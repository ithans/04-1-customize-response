package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.awt.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
public class CaseControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_204() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().is(204));
    }

    @Test
    void direct_return_message() throws Exception {
        mockMvc.perform(get("/api/messages/importance"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=utf-8"))
                .andExpect(content().string("importance"));
    }

    @Test
    void should_return_massage() throws Exception {
        mockMvc.perform(get("/api/message-objects/thing"))
                .andExpect(content().string("thing"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=utf-8"));
    }

    @Test
    void should_return_status_202() throws Exception {
        mockMvc.perform(get("/api/message-objects-with-annotation/things"))
                .andExpect(status().is(202))
                .andExpect(content().string("things"));
    }

    @Test
    void should_use_response() throws Exception {
        mockMvc.perform(get("/api/message-entities/entities"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=utf-8"))
                .andExpect(content().string("entities"));
    }
}
