package com.twuc.webApp.pojo;

public class Message {
    private final String value;

    public String getValue() {
        return value;
    }

    public Message(String value) {
        this.value = value;
    }
}
