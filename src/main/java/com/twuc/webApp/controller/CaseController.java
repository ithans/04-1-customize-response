package com.twuc.webApp.controller;

import com.twuc.webApp.pojo.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import sun.security.provider.certpath.OCSPResponse;

@RestController
public class CaseController {
    @GetMapping("/api/no-return-value")
    public void noReturnValue() {

    }

    @GetMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void noReturn() {

    }

    @GetMapping("/api/messages/{message}")
    public String getMessage(@PathVariable String message) {
        return message;
    }

    @GetMapping("/api/message-objects/{message}")
    public String getObjectMessage(@PathVariable Message message) {
        return message.getValue();
    }

    @GetMapping("/api/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public String getObjectWithAnnotation(@PathVariable Message message) {
        return message.getValue();
    }

    @GetMapping("/api/message-entities/{message}")
    public ResponseEntity getEntities(@PathVariable Message message){
       return ResponseEntity.status(200).body(message.getValue());
    }
}
